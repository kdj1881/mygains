export interface User{
    key?:string;
    userName:string;
    heightFeet:number;
    heightInches:number;
    weightPounds:number;

}