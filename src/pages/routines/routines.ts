import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ExercisesPage } from '../exercises/exercises';

@Component({
  selector: 'page-routines',
  templateUrl: 'routines.html',
})
export class RoutinesPage {
  myRoutines :any = {};
  objectKeys = Object.keys;
  weekdays = {};
  searchText: string;
  weekdayArray: Array<string>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, private storage: Storage) {
    this.weekdayArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    try{
      this.loadRoutines();
    }
    catch(error){

    }
  }

  loadRoutines(){
    this.storage.get("routines").then(data=>{
      this.myRoutines = data;
      
    });

    this.storage.get("weekdays").then(data=>{
      if(data == null){
        data = {
          "Sunday":"",
          "Monday": "",
          "Tuesday":"",
          "Wednesday":"",
          "Thursday":"",
          "Friday":"",
          "Saturday":""
          
        }
      }
      this.weekdays = data;
      
    });
  }

  ionViewDidLoad() {
    
  }

  printSets(sets:Array<any>){
    return sets.length + "x";
  }

  primaryMuscles(muscles:Array<any>){
    let output = "";
    muscles.forEach(data=>{
      output += " " + data.muscleDesc;
    });
    return output;
  }

  buildRoutine(){
    this.navCtrl.push(ExercisesPage, {"screenMode":"build"});
  }

  setWeekday(day, routineId){
    this.weekdays[day] = routineId;
    this.storage.set("weekdays",this.weekdays);
  }

  getItems(event) {
    let inputText = this.searchText || "";
    Object.keys(this.myRoutines).forEach(key => {
       
            this.myRoutines[key].hidden = false;
            if (!this.myRoutines[key].routineName.toLowerCase().includes(inputText)) {
              this.myRoutines[key].hidden = true;
            }
        
    });

}
}
