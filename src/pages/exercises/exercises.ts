import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import { RoutineBuildPage } from '../../pages/routine-build/routine-build';
import { MUSCLE_DATA } from '../../app/muscleData';
import { ExerciseInfoPage } from '../exercise-info/exercise-info';
@Component({
    selector: 'page-exercises',
    templateUrl: 'exercises.html'
})

export class ExercisesPage {
    items: any;
    checkedItems = {};
    searchText: string;
    screenMode: string;
    routineId: number;
    exercises: any;
    routineName: string;
    routineMode: string;
    dateWorkout: Date;
    dateWorkoutsArray: Array<any>;
    dateWorkoutIndex: number;
    constructor(public navParam: NavParams, public navCtrl: NavController, public toastCtrl: ToastController, public http: Http, private alertCtrl: AlertController) {
        this.checkedItems = {};
        this.screenMode = navParam.get("screenMode") || "readOnly";
        this.exercises = navParam.get("exercises");
        this.routineId = navParam.get("routineId");
        this.routineName = navParam.get("routineName");
        this.routineMode = navParam.get("routineMode");
        this.dateWorkout = navParam.get("dateWorkout") || new Date();
        this.dateWorkoutsArray = navParam.get("dayData") ||[];
        this.dateWorkoutIndex = navParam.get("index");

        if (this.exercises != null) {
            this.exercises.forEach(element => {
                this.checkedItems[element.exerciseId] = element;
            });
        }

        this.items = MUSCLE_DATA;

    }

    buildRoutine() {
        let anyChecked = false;
        let myArray = [];
        for (let key in this.checkedItems) {
            if (this.checkedItems[key].checked) {
                anyChecked = true;
                myArray.push(this.checkedItems[key]);
            }
        }
       
            
        
        if (anyChecked) {
            this.navCtrl.push(RoutineBuildPage, { 
                'myParam': myArray,
                "routineId": this.routineId,
                'routineName': this.routineName,
                'screenMode': this.routineMode,
                'dateWorkout': this.dateWorkout,
                'index': this.dateWorkoutIndex,
                'dayData': this.dateWorkoutsArray
            });
        }
        else {
            let alert = this.alertCtrl.create({
                title: 'Nothing Selected',
                subTitle: 'Please select one or more exercises/',
                buttons: ['Dismiss']
            });
            alert.present();
        }
    }

    updateSelected = function (obj, event) {
        obj.checked = event.value;
        this.checkedItems[obj.exerciseId] = obj;
        console.log(this.checkedItems);

    }
    isChecked(id) {
        return id in this.checkedItems;
    }
    notEmpty(obj) {
        Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    toggleGroup(group: any) {
        group.hidden = !group.hidden;
    }

    getItems(event) {
        let inputText = this.searchText || "";
        Object.keys(this.items).forEach(musclegroup => {
            Object.keys(this.items[musclegroup].exercises).forEach(exercise => {
                this.items[musclegroup].exercises[exercise].hidden = false;
                if (!this.items[musclegroup].exercises[exercise].exerciseTitle.toLowerCase().includes(inputText)) {
                    this.items[musclegroup].exercises[exercise].hidden = true;
                }
            });
        });

    }

    open(exercise: any) {
        console.log(exercise);
        this.navCtrl.push(ExerciseInfoPage, { exerciseData: exercise });
    }
}