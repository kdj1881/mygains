import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { StorageService } from '../../services/storage.service';
import { WorkoutPage } from '../workout/workout';
import { MonthStatsPage } from '../month-stats/month-stats';
/**
 * Generated class for the WorkoutCalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-workout-calendar',
  templateUrl: 'workout-calendar.html',
})
export class WorkoutCalendarPage {

  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  eventList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: StorageService) {
    this.date = new Date();
    this.monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Nov', 'Oct', 'Dec'];
    this.eventList = [];
  }


  ionViewDidEnter() {
    this.date = new Date();



  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkoutCalendarPage');
    this.getDaysOfMonth();
    this.loadEventThisMonth();
  }


  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    if (this.date.getMonth() === new Date().getMonth()) {
      this.currentDate = new Date().getDate();
    } else {
      this.currentDate = 999;
    }

    var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
    var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
    for (let i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
    for (let i = 0; i < thisNumOfDays; i++) {
      this.daysInThisMonth.push(i + 1);
    }

    var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
    var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
    for (let i = 0; i < (6 - lastDayThisMonth); i++) {
      this.daysInNextMonth.push(i + 1);
    }
    var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (let i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
        this.daysInNextMonth.push(i);
      }
    }
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
    this.loadEventThisMonth();
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
    this.loadEventThisMonth();
  }

  loadEventThisMonth() {

    this.storage.get(`workout/${this.date.getFullYear()}/${this.date.getMonth() + 1}`).then(data => {
      this.eventList = data || [];
    });


  }

  checkEvent(day) {
    return day in this.eventList;
  }

  viewDay(day) {
    if (day in this.eventList) {
      this.navCtrl.push(WorkoutPage, { eventData: this.eventList[day], eventDate: new Date(this.date.getFullYear(), this.date.getMonth(), day) });
    } else {
      this.navCtrl.push(WorkoutPage, { eventDate: new Date(this.date.getFullYear(), this.date.getMonth(), day) });
    }
  }

  viewMonthStats(eventList) {
    this.navCtrl.push(MonthStatsPage, { eventList: this.eventList });
  }

}
