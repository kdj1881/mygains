import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { StorageService } from '../../services/storage.service';
import { RoutinesPage } from '../routines/routines';
import { Chart } from 'chart.js';
import { ExercisesPage } from '../exercises/exercises';
import { Platform } from 'ionic-angular';
import { WorkoutCalendarPage } from '../workout-calendar/workout-calendar';

@Component({
  selector: 'page-routine-build',
  templateUrl: 'routine-build.html',
})
export class RoutineBuildPage {
  @ViewChild('barCanvas') barCanvas;
  routineId: number;
  myRoutineData: any;
  exercisePbs: any;
  muscleBreakdown: Object;
  routineName: String;
  devWidth: number;
  dateWorkout: Date;

  dateWorkoutsArray: Array<any>;
  dateWorkoutIndex: number;
  /* Screen Modes:
      build: building a routine for reuse or editing an existing
      workout: doing a workout whether that is from a routine or adhoc
      workoutEdit: editing an existing workout
  */
 screenMode: string;
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private storage: StorageService,
      private alertCtrl: AlertController,
      public plt: Platform,
      public modal: ModalController
  ) {
    /* init params */
    this.muscleBreakdown = {};
    this.myRoutineData = navParams.get('myParam') || [];
    this.routineId = navParams.get("routineId");
    this.screenMode = navParams.get('screenMode') || "build";
    this.routineName = navParams.get('routineName') || "New Workout";
    let routine = navParams.get("routine");
    this.devWidth = this.plt.width();
    this.dateWorkout = navParams.get('dateWorkout') || new Date();
    this.dateWorkoutsArray = navParams.get("dayData") ||[];
    this.dateWorkoutIndex = navParams.get("index");
    console.log(this.dateWorkoutIndex);
    if (this.routineId != null && routine != null) {

      this.myRoutineData = routine.exercises;
      this.routineName = routine.routineName;
    }


    if(this.dateWorkoutsArray.length > 0 && this.dateWorkoutIndex !=null){
      if(this.myRoutineData.length > 0){
        for (let item of this.myRoutineData) {
          // for each selected exercise see if its already there if not add it
          let found = false;
          this.dateWorkoutsArray[this.dateWorkoutIndex].exercise.forEach(exercise => {
            if(exercise.exerciseId == item.exerciseId){
              found =true;
            }
          });
          if(!found){
            this.dateWorkoutsArray[this.dateWorkoutIndex].exercise.push(item);
          }
        }
      }
      this.myRoutineData = this.dateWorkoutsArray[this.dateWorkoutIndex].exercise;
      this.routineName ="Edit workout";
    }


    /* get exercise personal bests */
    this.storage.get("pb").then(pb => {
      this.exercisePbs = pb || {};
    });


    for (let data of this.myRoutineData) {
      data.primaryMuscles.forEach(muscles => {
        if (!(muscles.muscleId in this.muscleBreakdown)) {
          this.muscleBreakdown[muscles.muscleId] = {
            "total": 0,
            "desc": muscles.muscleDesc
          };
        }
        this.muscleBreakdown[muscles.muscleId].total++;
      });

      data.secondaryMuscles.forEach(muscles => {
        if (!(muscles.muscleId in this.muscleBreakdown)) {
          this.muscleBreakdown[muscles.muscleId] = {
            "total": 0,
            "desc": muscles.muscleDesc
          };
        }
        this.muscleBreakdown[muscles.muscleId].total += 0.5;
      });
      if (data.sets == null || data.sets.length == 0) {
        data.sets = [
          {
            "no": 1,
            "weight": '',
            "reps": ''
          },
          {
            "no": 2,
            "weight": '',
            "reps": ''
          },
          {
            "no": 3,
            "weight": '',
            "reps": ''
          }
        ]
      }
    }



  }

  ionViewDidLoad() {
    this.drawMuscleGraph();
  }
  getDate() {
    return this.dateWorkout;
  }
  addSet(setArray: Array<any>) {
    let newSet = {
      "no": setArray.length + 1,
      "weight": setArray[setArray.length - 1].weight,
      "reps": setArray[setArray.length - 1].reps
    }
    setArray.push(newSet);
  }

  removeSet(setArray: Array<any>){
    if(setArray.length > 1){
      setArray.pop();
    }
  }

  getWeight(setArray: Array<any>) {
    let sum: number;
    sum = 0;
    setArray.forEach(set => {
      sum += ((Number(set.weight) || 0) * (Number(set.reps) || 0));
    });
    return sum;
  }

  getReps(setArray: Array<any>) {
    let sum = 0;
    setArray.forEach(set => {
      sum += Number(set.reps) || 0;
    });
    return sum;
  }

  getExercisePB(exerciseId) {
    if (this.exercisePbs == null || typeof this.exercisePbs == 'undefined' || !this.exercisePbs.hasOwnProperty(exerciseId)) {
      return 0;
    }
    else {
      return Number(this.exercisePbs[exerciseId]) || 0;
    }
  }

  removeExercise(exerciseId) {
    let alert = this.confirmationAlert("Are you sure?");

    alert.then((data) => {
      if (data) {
        console.log("remove");
        for (var i = 0; i <= this.myRoutineData.length; i++) {
          if (this.myRoutineData[i].exerciseId === exerciseId) {
            this.myRoutineData.splice(i, 1);
            break;
          }
        }
      }
    });
  }

  saveRoutine() {
    let routine = {
      exercises: this.myRoutineData,
      routineName: this.routineName
    };
    this.storage.get('routines').then(data => {
      data = data || {};

      if (this.routineId != null) {
        data[this.routineId] = routine;
      }
      else {
        this.routineId = this.getNextId(data);
      }
      data[this.routineId] = routine;
      this.storage.set('routines', data);
    });
    this.navCtrl.popToRoot().then(value => {
      this.navCtrl.push(RoutinesPage);
    });
  }

  getNextId(data) {
    var i = 1;
    while (i in data) {
      i++;
    }
    return i;
  }

  DeleteRoutine() {

    let alert = this.confirmationAlert("Are you sure?");
    alert.then((data) => {
      if (data) {
        if(this.dateWorkoutsArray.length >= 1 && this.dateWorkoutIndex !=null){
          let now = this.dateWorkout;
          let day = now.getDate();
          let month = now.getMonth() + 1;
          let year = now.getFullYear();
          console.log(`workout/${year}/${month}`);
          console.log(this.dateWorkoutIndex);
          this.dateWorkoutsArray.splice(this.dateWorkoutIndex, 1);
          console.log(this.dateWorkoutsArray)
          this.storage.get(`workout/${year}/${month}`).then(data => {
            data[day] =  this.dateWorkoutsArray
            this.storage.set(`workout/${year}/${month}`, data);
            this.navCtrl.popToRoot();
            this.navCtrl.push(WorkoutCalendarPage);
            return;
          });
        }else{
          this.storage.get('routines').then(data => {
            data = data || {};
            if (this.routineId != null) {
              delete data[this.routineId];
              this.storage.set('routines', data);
            }
          });
          
          this.navCtrl.popToRoot().then(value => {
            this.navCtrl.push(RoutinesPage);
          });
        }

       
      }
    });

  }

  EditExercise() {
    let routine = {
      exercises: this.myRoutineData,
      routineId: this.routineId,
      routineName: this.routineName,
      routineMode: this.screenMode,
      dateWorkout: this.dateWorkout,
      index: this.dateWorkoutIndex,
      dayData: this.dateWorkoutsArray,
      screenMode: "build"
    };

    if (this.screenMode != 'build' || this.routineId != null) {
      // .. send data to exercise screen
      this.navCtrl.push(ExercisesPage, routine);
    }
    else {
      // .. go back
      this.navCtrl.pop();
    }
  }

  private confirmationAlert(message: string): Promise<boolean> {
    let resolveFunction: (confirm: boolean) => void;
    let promise = new Promise<boolean>(resolve => {
      resolveFunction = resolve;
    });
    let alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: message,
      enableBackdropDismiss: false,
      buttons: [{
        text: 'No',
        handler: () => resolveFunction(false)
      }, {
        text: 'Yes',
        handler: () => resolveFunction(true)
      }]
    });
    alert.present();
    return promise;
  }
  saveWorkout() {
    let alert = this.confirmationAlert("All done?");
    alert.then((resp) => {
      if (resp) {
        let now = this.dateWorkout;
        let month = now.getMonth() + 1;
        let year = now.getFullYear();
        let day = now.getDate();
        let workout = {
          date: year + "-" + month + "-" + day,
          totals: this.getWorkoutTotals(),
          exercise: this.myRoutineData
        }

        this.storage.get(`workout/${year}/${month}`).then(data => {
          data = data || {};
          if (day in data) {
            // ..if workout data exist append
            if (!Array.isArray(data[day])) {
              data[day] = [];

            }
            workout.exercise = this.myRoutineData;
            if(this.dateWorkoutsArray.length >= 1 && this.dateWorkoutIndex != null){
              data[day][this.dateWorkoutIndex] = workout;
              this.storage.set(`workout/${year}/${month}`, data);
            }
            else{
              data[day].push(workout);
            }
            
          } else {
            // ..if workout data not exist insert 
            data[day] = [workout];
          }
          this.storage.set(`workout/${year}/${month}`, data);
        });
        this.navCtrl.popToRoot();
        this.navCtrl.push(WorkoutCalendarPage);
      }
    });

  }
  saveRoutinePB(volumn) {
    this.storage.get('routines').then(data => {

      if ("PB" in data[this.routineId]) {
        if (volumn > data[this.routineId].PB) {
          data[this.routineId].PB = volumn;
        }
      }
      else {
        data[this.routineId].PB = volumn;
      }
      this.storage.set('routines', data);
    });

  }
  getWorkoutTotals() {
    let totals = {
      reps: 0,
      volumn: 0,
      muscleBreakdown: this.muscleBreakdown
    };
    this.myRoutineData.forEach(exercise => {
      totals.reps += this.getReps(exercise.sets);
      totals.volumn += this.getWeight(exercise.sets);
      if (!(exercise.exerciseId in this.exercisePbs) || this.getWeight(exercise.sets) > this.exercisePbs[exercise.exerciseId]) {
        this.exercisePbs[exercise.exerciseId] = this.getWeight(exercise.sets);
      }
    });
    this.storage.set("pb", this.exercisePbs);
    if (this.routineId != null) {
      this.saveRoutinePB(totals.volumn);
    }

    return totals;
  }

  private drawMuscleGraph() {
    var chartData = [];
    var chartLabels = [];
    var chartColor = [];
    var corlorBorders = [];

    Object.keys(this.muscleBreakdown).forEach(key => {
      chartLabels.push(this.muscleBreakdown[key].desc);
      chartData.push(this.muscleBreakdown[key].total);
      var newColor = this.dynamicColors();
      chartColor.push(newColor.color);
      corlorBorders.push(newColor.border);
    });

    new Chart(this.barCanvas.nativeElement, {

      type: 'horizontalBar',
      data: {
        labels: chartLabels,
        datasets: [
          {

            data: chartData,
            backgroundColor: chartColor,
            borderColor: corlorBorders,
            borderWidth: 1
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        title: {
          display: true,
          text: 'Primary Muscle: 1 | Secondary Muscle: 0.5',
          position: "bottom"
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            barThickness: 73,
            categorySpacing: 5,
            categoryPercentage: 1.5,
            barPercentage: 1.5
          }]
        }
      }

    });
  }

  dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return {
      color: "rgb(" + r + "," + g + "," + b + ", 0.2)",
      border: "rgb(" + r + "," + g + "," + b + ", 1)"
    };
  }

  minus(weight) {
    weight = weight - 1 || 0;
    console.log(weight);
  }
  plus(weight) {
    weight = weight + 1 || 1;
  }

  Number(num) {
    return Number(num);
  }

  toggleGroup(group: any) {
    group.hidden = !group.hidden;
  }
}
