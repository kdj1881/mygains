import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { UserInfoPage } from '../user-info/user-info';
import { StorageService } from '../../services/storage.service';
import { ExercisesPage } from '../exercises/exercises';
import { RoutinesPage } from '../routines/routines';
import { WorkoutCalendarPage } from '../workout-calendar/workout-calendar';
import { MUSCLES_STRUCT } from '../../app/muscleStruct';
import * as moment from 'moment';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('muscleCanvas') muscleCanvas;

  barChart: any;
  radarChart: any;
  pushPage: any;
  userInfo: any = {};
  todaysRoutine: any = {};
  todaysRoutineId: number;
  todayWeekday: string = "";
  eventData: any;
  weekVolumn: any;
  weekMuscleGroupHits: any;

  constructor(public navCtrl: NavController, public storage: StorageService) {
    this.eventData = [];

  }

  ionViewDidEnter() {
    this.checkSetup();
    this.getTodaysRoutine();
    this.loadEventThisWeek().then(data => {
      this.buildBarChart();
    });

  }
  openUser() {
    this.navCtrl.push(UserInfoPage);
  }
  openRoutines() {
    this.navCtrl.push(RoutinesPage);
  }
  openExercises() {
    this.navCtrl.push(ExercisesPage, { "screenMode": "" });
  }
  checkSetup() {
    this.storage.get('userInfo').then((userInfo) => {
      if (userInfo === null) {
        this.navCtrl.push(UserInfoPage);
      } else {
        this.userInfo = userInfo;
      }
    });
  }
  getTodaysRoutine() {
    this.storage.get('weekdays').then((week) => {
      if (week !== null) {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var d = new Date();
        var dayName = days[d.getDay()];
        this.todayWeekday = dayName;
        if (dayName in week) {
          this.storage.get("routines").then(data => {
            if (week[dayName] in data) {
              data[week[dayName]].routineName = data[week[dayName]].routineName;
              this.todaysRoutine = data[week[dayName]];
              this.todaysRoutineId = week[dayName];
            }
          });
        }
      }
    });
  }

  async loadEventThisWeek() {
    var date = new Date();

    var week = this.days(date);

    var nextMonthDays = [];
    var thisMonthDays = [];
    week.forEach(day => {
      if (day >= week[0]) {
        thisMonthDays.push(day);
      }
      else {
        nextMonthDays.push(day);
      }
    });


    this.eventData = [];
    await this.storage.get(`workout/${date.getFullYear()}/${date.getMonth() + 1}`).then(data => {

      if (data != null) {
        thisMonthDays.forEach(day => {
          if (day in data) {
            this.eventData.push(data[day]);
          }
        });
      }
    });
    if (nextMonthDays.length > 0) {
      await this.storage.get(`workout/${date.getFullYear()}/${date.getMonth() + 2}`).then(data => {

        if (data != null) {
          nextMonthDays.forEach(day => {
            if (day in data) {
              this.eventData.push(data[day]);
            }
          });
        }
      });
    }




  }

  days(current) {
    var startOfWeek = moment(current).startOf('isoWeek');
    var endOfWeek = moment(current).endOf('isoWeek');

    var days = [];
    var day = startOfWeek;

    while (day <= endOfWeek) {
      var tempDate = day.toDate();
      days.push(tempDate.getDate());
      day = day.clone().add(1, 'd');
    }
    return days;
  }

  openHistory() {
    this.navCtrl.push(WorkoutCalendarPage);
  }
  ionViewDidLoad() {

  }

  buildBarChart() {
    var chartData = [];
    var chartLabels = [];
    var chartColor = [];
    var corlorBorders = [];
    var weekVolumn = [];
    var muscleData = {};
    for (var i = 0; i < 7; i++) {
      weekVolumn[i] = 0;
    }
    try {
      this.eventData.forEach(event => {
        event.forEach(element => {
          var weekday = moment(element.date).isoWeekday();
          weekVolumn[weekday] += element.totals.volumn;
          Object.keys(element.totals.muscleBreakdown).forEach(key => {
            let thisObj = element.totals.muscleBreakdown[key];
            if (!(thisObj.desc in muscleData)) {
              muscleData[thisObj.desc] = 0;
            }
            muscleData[thisObj.desc] += thisObj.total;
          });


        });


      });

      for (let i = 1; i < Object.keys(MUSCLES_STRUCT.muscles).length; i++) {
        let thisMuscle = MUSCLES_STRUCT.muscles[i];
        chartLabels.push(thisMuscle.muscleDesc);
        if (thisMuscle.muscleDesc in muscleData) {
          // .. do nothing
        }
        else {
          muscleData[thisMuscle.muscleDesc] = 0;
          chartData.push();
        }
        chartData.push(muscleData[thisMuscle.muscleDesc]);
        var newColor = this.dynamicColors();
        chartColor.push(newColor.color);
        corlorBorders.push(newColor.border);
      }

      var barChart = new Chart(this.muscleCanvas.nativeElement, {

        type: 'horizontalBar',
        data: {
          labels: chartLabels,
          datasets: [
            {

              data: chartData,
              backgroundColor: chartColor,
              borderColor: corlorBorders,
              borderWidth: 1
            }
          ]
        },
        options: {
          maintainAspectRatio: false,
          title: {
            display: true,
            text: 'Primary Muscle: 1 | Secondary Muscle: 0.5',
            position: "bottom"
          },
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              categorySpacing: 5,
              ticks: {
                beginAtZero: true,
                categoryPercentage: 1.5,
                barPercentage: 1.5,
                barThickness: 73
              }
            }],
            yAxes: [{
              categorySpacing: 5,
              ticks: {
                beginAtZero: true,
                categoryPercentage: 1.5,
                barPercentage: 1.5,
                barThickness: 73
              }
            }]
          }
        }

      });


      this.barChart = new Chart(this.barCanvas.nativeElement, {

        type: 'line',
        data: {
          labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          datasets: [{
            label: "",
            backgroundColor: 'rgba(54, 162, 235, 0.2)',
            borderColor: 'rgba(54, 162, 235, 1)',
            pointBackgroundColor: 'rgba(255,99,132,.5)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(255,99,132,.5)',
            data: weekVolumn
          }]
        },
        options: {
          title: {
            display: true,
            text: '',
            position: "bottom"
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                categoryPercentage: 1.5,
                barPercentage: 1.5
              }
            }],
            xAxes: [{
              categorySpacing: 5,
              ticks: {
                beginAtZero: true,
                categoryPercentage: 1.5,
                barPercentage: 1.5,
                barThickness: 73
              }
            }]
          },
          legend: {
            display: false
          }
        }

      });
    } catch (e) {
      console.log(e);
    }
  }
  dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return {
      color: "rgb(" + r + "," + g + "," + b + ", 0.2)",
      border: "rgb(" + r + "," + g + "," + b + ", 1)"
    };
  }

}
