import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MUSCLES_STRUCT } from '../../app/muscleStruct';

@Component({
  selector: 'page-exercise-info',
  templateUrl: 'exercise-info.html',
})
export class ExerciseInfoPage {

  exerciseData: any;
  muscleData: any = MUSCLES_STRUCT.muscles;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.exerciseData = navParams.get("exerciseData");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExerciseInfoPage');
  }

  ObjectKeys(obj: any) {
    return Object.keys(obj);
  }
  outputDesc(desc) {
    return desc.replace(/\.(?=[A-Z])/g, '\n');
  }
  isSelected(obj: Array<any>, muscleId: string) {
    console.log(obj);
    console.log(muscleId);
    obj.forEach((f) => {
      if (Number(f.muscleId) === Number(muscleId)) {
        console.log("true");
        return true;
      }
    });
    return false;
  }

}
