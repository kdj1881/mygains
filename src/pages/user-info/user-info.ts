import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { User } from '../../models/user.model';
import { StorageService } from '../../services/storage.service';


@Component({
  selector: 'page-user-info',
  templateUrl: 'user-info.html',
})
export class UserInfoPage {
  userInfo  : User;
  userStorage: Storage;
  userKey: any;
  toast: ToastController;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: StorageService, public toastController: ToastController) {
    
    var key = storage.get("userInfo").then(promise =>{
      
      if(typeof promise == 'undefined' || promise == null){
        this.userInfo ={
            userName:"",
            heightFeet:null,
            heightInches:null,
            weightPounds:null
         }
      }else{
        this.userInfo= promise;
      }
      
    });
   
    
    
  }

  ionViewDidLoad() {
    
  }

  addUser(user: User){
      
      this.storage.set("userInfo",user).then(promise=>{
        this.presentToast();
        this.navCtrl.pop();
      });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your User Info has been saved.',
      duration: 2000
    });
    toast.present();
  }

 
}
