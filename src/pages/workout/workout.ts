import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { MUSCLES_STRUCT } from '../../app/muscleStruct';
import { RoutineBuildPage } from '../routine-build/routine-build';
@Component({
  selector: 'page-workout',
  templateUrl: 'workout.html',
})
export class WorkoutPage {

  @ViewChild('barCanvas') barCanvas;
  eventData: any;
  barChart: any;
  eventDate: Date;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.eventData = this.navParams.get("eventData") || [];
    this.eventDate = this.navParams.get("eventDate") || new Date();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkoutPage');
    this.buildBarChart();
  }


  getWeight(setArray: Array<any>) {
    let sum: number;
    sum = 0;
    setArray.forEach(set => {
      sum += ((Number(set.weight) || 0) * (Number(set.reps) || 0));
    });
    return sum;
  }

  getReps(setArray: Array<any>) {
    let sum = 0;
    setArray.forEach(set => {
      sum += Number(set.reps) || 0;
    });
    return sum;
  }

  buildBarChart() {
    var chartData = [];
    var chartLabels = [];
    var chartColor = [];
    var corlorBorders = [];
    var muscleData = {};
    try {
      this.eventData.forEach(event => {
        Object.keys(event.totals.muscleBreakdown).forEach(key => {
          let thisObj = event.totals.muscleBreakdown[key];
          if (!(thisObj.desc in muscleData)) {
            muscleData[thisObj.desc] = 0;
          }
          muscleData[thisObj.desc] += thisObj.total;
        });


      });

      for (let i = 1; i < Object.keys(MUSCLES_STRUCT.muscles).length; i++) {
        let thisMuscle = MUSCLES_STRUCT.muscles[i];
        chartLabels.push(thisMuscle.muscleDesc);
        if (thisMuscle.muscleDesc in muscleData) {
          // .. do nothing
        }
        else {
          muscleData[thisMuscle.muscleDesc] = 0;
          chartData.push();
        }
        chartData.push(muscleData[thisMuscle.muscleDesc]);
        var newColor = this.dynamicColors();
        chartColor.push(newColor.color);
        corlorBorders.push(newColor.border);
      }


      var barChart = new Chart(this.barCanvas.nativeElement, {

        type: 'horizontalBar',
        data: {
          labels: chartLabels,
          datasets: [
            {

              data: chartData,
              backgroundColor: chartColor,
              borderColor: corlorBorders,
              borderWidth: 1
            }
          ]
        },
        options: {
          maintainAspectRatio: false,
          title: {
            display: true,
            text: 'Primary Muscle: 1 | Secondary Muscle: 0.5',
            position: "bottom"
          },
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              categorySpacing: 5,
              ticks: {
                beginAtZero: true,
                categoryPercentage: 1.5,
                barPercentage: 1.5,
                barThickness: 73
              }
            }]
          }
        }

      });
    } catch (e) {
      console.log(e);
    }
  }
  dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return {
      color: "rgb(" + r + "," + g + "," + b + ", 0.2)",
      border: "rgb(" + r + "," + g + "," + b + ", 1)"
    };
  }

  addWorkout() {
    this.navCtrl.push(RoutineBuildPage, { 'screenMode': 'workout', dateWorkout: this.eventDate });
  }

  EditWorkout(index:number, data:any){
    this.navCtrl.push(RoutineBuildPage, { 'screenMode': 'workoutEdit', 'dateWorkout': this.eventDate,  'index': index, 'dayData': data});
  }
}
