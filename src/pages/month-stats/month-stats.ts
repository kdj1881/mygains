import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { MUSCLES_STRUCT } from '../../app/muscleStruct';
/**
 * Generated class for the MonthStatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-month-stats',
  templateUrl: 'month-stats.html',
})
export class MonthStatsPage {
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('volumeCanvas') volumeCanvas;
  eventList:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.eventList = navParams.get("eventList");
  }

  ionViewDidLoad() {
    this.buildBarChart();
  }

  getWeight(setArray: Array<any>){
    let sum : number;
    sum = 0;
    setArray.forEach(set => {
      sum += ((Number(set.weight) || 0) * (Number(set.reps) || 0));
    });
    return sum;
  }

  getReps(setArray: Array<any>){
    let sum =0;
    setArray.forEach(set => {
      sum += Number(set.reps) || 0;
    });
    return sum;
  }

  buildBarChart(){
    var chartData = [];
    var chartLabels = [];
    var chartColor = [];
    var corlorBorders = [];
    var muscleData ={};
    var monthVolumn = [];
    var monthDates = [];
    try{
      var dateTrack = 0;
      Object.keys(this.eventList).forEach(key => {
        let eventData = this.eventList[key];
        monthVolumn[dateTrack] = 0;
        eventData.forEach(event => {
          Object.keys(event.totals.muscleBreakdown).forEach(key => {
            let thisObj = event.totals.muscleBreakdown[key];

            if(!(thisObj.desc in muscleData)){
              muscleData[thisObj.desc] = 0;
            }
            muscleData[thisObj.desc] += thisObj.total;
          });
          
          monthDates[dateTrack] = event.date;
          monthVolumn[dateTrack] += event.totals.volumn;
        });
        dateTrack++;
      });
     

      for(let i = 1; i < Object.keys(MUSCLES_STRUCT.muscles).length; i ++){
        let thisMuscle = MUSCLES_STRUCT.muscles[i];
        chartLabels.push(thisMuscle.muscleDesc);
        if(thisMuscle.muscleDesc in muscleData){
          // .. do nothing
        }
        else{
          muscleData[thisMuscle.muscleDesc] = 0;
          chartData.push();
        }
        chartData.push(muscleData[thisMuscle.muscleDesc]);
        var newColor = this.dynamicColors();
        chartColor.push(newColor.color);
        corlorBorders.push(newColor.border);
      }
      

      var barChart = new Chart(this.barCanvas.nativeElement, {

        type: 'horizontalBar',
        data: {
            labels: chartLabels,
            datasets: [
                {
                    
                    data: chartData,
                    backgroundColor: chartColor,
                    borderColor: corlorBorders,
                    borderWidth: 1
                }
            ]
          },
          options: {
            maintainAspectRatio: false,
            title: {
              display: true,
              text: 'Primary Muscle: 1 | Secondary Muscle: 0.5',
              position: "bottom"
            },
            legend: {
              display: false 
            },
            scales:{
              xAxes:[{
                categorySpacing: 5,
                ticks: {
                  beginAtZero: true,
                  categoryPercentage: 1.5,
                  barPercentage: 1.5,
                  barThickness: 73
                }
              }]
            }
          }

      });

      new Chart(this.volumeCanvas.nativeElement, {

        type: 'line',
        data: {
            labels: monthDates,
            datasets: [{
               label:"",
               backgroundColor: 'rgba(54, 162, 235, 0.2)',
              borderColor: 'rgba(54, 162, 235, 1)',
              pointBackgroundColor: 'rgba(255,99,132,.5)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(255,99,132,.5)',
              data: monthVolumn
            }]
        },
        options: {
          title: {
              display: true,
              text: '',
              position: "bottom"
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        categoryPercentage: 1.5,
                        barPercentage: 1.5
                    }
                }],
                xAxes:[{
                  categorySpacing: 5,
                  ticks: {
                    beginAtZero: true,
                    categoryPercentage: 1.5,
                    barPercentage: 1.5,
                    barThickness: 73
                  }
                }]
            },
          legend: {
            display: false 
          }
        }

    });
    }catch(e){
      console.log(e);
    }
  }
  dynamicColors(){
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return {
      color: "rgb(" + r + "," + g + "," + b + ", 0.2)",
      border:"rgb(" + r + "," + g + "," + b + ", 1)"
    };
  }

}
