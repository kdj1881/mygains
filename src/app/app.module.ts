import { BrowserModule, } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA, } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen, } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ExercisesPage } from '../pages/exercises/exercises';
import { RoutinesPage } from '../pages/routines/routines';
import { WorkoutPage } from '../pages/workout/workout';
import { UserInfoPage } from '../pages/user-info/user-info';
import { RoutineBuildPage } from '../pages/routine-build/routine-build';
import { WorkoutCalendarPage } from '../pages/workout-calendar/workout-calendar';
import { MonthStatsPage } from '../pages/month-stats/month-stats';
import { ExerciseInfoPage } from '../pages/exercise-info/exercise-info';

import { HttpModule } from '@angular/http';
import { StorageService } from '../services/storage.service';
import { RoutineGridComponent } from '../components/routine-grid/routine-grid';
/* define all pages, components, or directive they need to be added here */

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    MyApp,
    HomePage,
    ExercisesPage,
    RoutinesPage,
    WorkoutPage,
    RoutineBuildPage,
    WorkoutCalendarPage,
    UserInfoPage,
    MonthStatsPage,
    RoutineGridComponent,
    ExerciseInfoPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, { scrollAssist: false, autoFocusAssist: false })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ExercisesPage,
    RoutinesPage,
    WorkoutPage,
    RoutineBuildPage,
    UserInfoPage,
    WorkoutCalendarPage,
    MonthStatsPage,
    ExerciseInfoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    StorageService
  ],
  exports: [RoutineGridComponent]
})
export class AppModule { }
