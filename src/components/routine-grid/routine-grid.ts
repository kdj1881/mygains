import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RoutineBuildPage } from '../../pages/routine-build/routine-build';

@Component({
  selector: 'routine-grid',
  templateUrl: 'routine-grid.html'
})
export class RoutineGridComponent {
    @Input('RoutineId') RoutineId;
    @Input('Routine') Routine;

  constructor(private navController: NavController) {

  }

  printSets(sets:Array<any>){
    return sets.length + "x";
  }

  primaryMuscles(muscles:Array<any>){
    let output = "";
    muscles.forEach(data=>{
      output += " " + data.muscleDesc;
    });
    return output;
  }
  
  editRoutine(){
    console.log(this.Routine);
    this.navController.push(RoutineBuildPage, {"routine": this.Routine, "routineId": this.RoutineId });
  }

  startRoutine(){
    
    this.navController.push(RoutineBuildPage, {"routine": this.Routine, "routineId": this.RoutineId, screenMode:"workout" });
  }

}