import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageService {

    constructor(private storageIonicRef: Storage) {

    }

    get(key: string): Promise<any> {
        return this.storageIonicRef.get(key);
    }

    set(key: string, data: any): Promise<any> {
        return this.storageIonicRef.set(key, data);
    }

    delete(key: string): Promise<any> {
        return this.storageIonicRef.remove(key);
    }
}